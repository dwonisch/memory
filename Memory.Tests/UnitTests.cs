using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Memory.Tests {
    [TestClass]
    public class UnitTests {
        private byte[] sampleData = new byte[] { 129, 127 };

        [TestMethod]
        public void ReadSingleByte() {
            var pointer = Marshal.AllocHGlobal(sampleData.Length);
            
            try {
                var currentProcess = System.Diagnostics.Process.GetCurrentProcess();
                if (currentProcess == null)
                    throw new ArgumentNullException("currentProcess");

                Memory.WriteByte(currentProcess.Handle, pointer, sampleData);

                Assert.AreEqual(129, Memory.ReadByte(currentProcess.Handle, pointer));
                Assert.AreEqual(127, Memory.ReadByte(currentProcess.Handle, pointer + 1));

            } finally {
                Marshal.FreeHGlobal(new IntPtr((long)pointer));
            }
        }
    }
}