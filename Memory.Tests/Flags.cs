﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Memory.Tests {
    [TestClass]
    public class Flags {
        [DataTestMethod]
        [DataRow(0, 0, false)]
        [DataRow(1,0,true)]
        [DataRow(2, 0, false)]
        [DataRow(2, 1, true)]
        [DataRow(3, 0, true)]
        [DataRow(3, 1, true)]
        public void CheckFlags(int number, int flag, bool expectedResult) {
            Assert.AreEqual(expectedResult, Memory.GetFlag(number, flag));
        }

        [DataTestMethod]
        [DataRow(0, 0, false)]
        [DataRow(1, 0, true)]
        [DataRow(2, 0, false)]
        [DataRow(2, 1, true)]
        [DataRow(3, 0, true)]
        [DataRow(3, 1, true)]
        public void CheckFlagsFromByteArray(int number, int flag, bool expectedResult) {
            Assert.AreEqual(expectedResult, Memory.GetFlag(BitConverter.GetBytes(number), flag));
        }

        [DataTestMethod]
        [DataRow(new byte[]{0,0,0,0,1,0,0,0}, 32, true)]
        [DataRow(new byte[] { 0, 0, 0, 0, 1,1,1,1 }, 32, true)]

        public void CheckFlagsFromByteArrayComplex(byte[] number, int flag, bool expectedResult) {
            Assert.AreEqual(expectedResult, Memory.GetFlag(number, flag));
        }

        [DataTestMethod]
        [DataRow(new byte[] { 0, 0, 0, 0 }, 33)]
        public void OutOfRange(byte[] number, int flag) {
            Assert.ThrowsException<IndexOutOfRangeException>(() => Memory.GetFlag(number, flag));

        }
    }
}
