﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using Memory.Native;

namespace Memory {
    public class ProcessMemory : MemoryBase, IDisposable {
        public int MaximumNumberOfModulesLoaded { get; set; } = 255;
        public int MaximumModuleNameLength { get; set; } = 255;

        private ModuleInfo mainModule;
        private Dictionary<string, ModuleInfo> moduleAddresses;

        public ProcessMemory(int processId, ProcessAccessFlags flags) : base(Open(processId, flags), 0) {
        }

        public ProcessModuleMemory GetMainModule() {
            if (moduleAddresses == null) {
                InitializeModules();
            }

            return new ProcessModuleMemory(handle, mainModule.Base.ToInt64());
        }

        public ProcessModuleMemory GetModule(string module) {
            if (moduleAddresses == null) {
                InitializeModules();
            }

            if (module.ToLower() == "base") {
                return GetMainModule();
            }

            if (!moduleAddresses.ContainsKey(module)) {
                return null;
            }

            return new ProcessModuleMemory(handle, moduleAddresses[module].Base.ToInt64());
        }

        public string GetFilePath() {
            var buffer = new byte[1024];
            var length = Windows.GetProcessImageFileNameA(handle, buffer, buffer.Length);
            var devicePath = Encoding.Default.GetString(buffer, 0, length);
            //devicePath should look like \Device\HarddiskVolume5\Program Files\...

            Trace.WriteLine($"Device Path: {devicePath}");
            foreach (var drive in DriveInfo.GetDrives()) {
                var shortDrive = "O:";
                var path = GetDevicePath(shortDrive);

                Trace.WriteLine($"Path (new): {shortDrive} - {path}");

                if (devicePath.StartsWith(path))
                    return devicePath.Replace(path, shortDrive);
            }

            return devicePath;
        }

        private string GetDevicePath(string path) {
            var b = new StringBuilder(1024);
            Windows.QueryDosDevice(path.Substring(0, 2), b, b.Capacity);
            return b.ToString();
        }

        public string GetFileVersion() {
            var path = GetFilePath();
            return File.Exists(path) ? FileVersionInfo.GetVersionInfo(path).FileVersion : null;
        }

        public static IntPtr Open(int id, ProcessAccessFlags flags) {
            return Windows.OpenProcess((int)flags, false, id);
        }

        public static ProcessMemory OpenReadQuery(int processId) {
            return new ProcessMemory(processId, ProcessAccessFlags.VirtualMemoryRead | ProcessAccessFlags.QueryInformation);
        }

        public static ProcessMemory OpenRead(int processId) {
            return new ProcessMemory(processId, ProcessAccessFlags.VirtualMemoryRead);
        }

        public void Dispose() {
            if(handle != IntPtr.Zero)
                Windows.CloseHandle(handle);
        }

        private void InitializeModules() {
            mainModule = new ModuleInfo();
            moduleAddresses = new Dictionary<string, ModuleInfo>();

            var buffer = new IntPtr[MaximumNumberOfModulesLoaded];
            var size = Marshal.SizeOf<IntPtr>();

            Windows.EnumProcessModulesEx(handle, buffer, size * buffer.Length, out int dataLength, ModulesFilterFlag.All);

            for (int i = 0; i < dataLength / size; i++) {
                var moduleInfo = Memory.GetModuleInformation(handle, buffer[i]);
                var moduleName = Memory.GetModuleName(handle, buffer[i], MaximumModuleNameLength);

                if (i == 0) {
                    mainModule = moduleInfo;
                }

                moduleAddresses.Add(moduleName, moduleInfo);
            }
        }
    }
}
