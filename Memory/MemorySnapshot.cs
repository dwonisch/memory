﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Schema;

namespace Memory {
    public class MemorySnapshot : IMemory {
        private readonly IMemory memory;

        private class MemoryIndex {
            public IntPtr Address;
            public int Length;
        }

        private readonly Dictionary<MemoryIndex, int> queriedMemory = new Dictionary<MemoryIndex, int>();
        private readonly Dictionary<IntPtr, byte[]> snapshot = new Dictionary<IntPtr, byte[]>();

        public MemorySnapshot(IMemory memory) {
            this.memory = memory;
        }

        public byte ReadByte(IntPtr address) {
            return ReadByte(address, 1)[0];
        }

        public byte[] ReadByte(IntPtr address, int length) {
            if (TryResolve(address, length, out var data))
                return data;
            return Update(address, length);
        }

        public short ReadInt16(IntPtr address) {
            throw new NotImplementedException();
        }

        public ushort ReadUInt16(IntPtr address) {
            throw new NotImplementedException();
        }

        public int ReadInt32(IntPtr address) {
            throw new NotImplementedException();
        }

        public uint ReadUInt32(IntPtr address) {
            throw new NotImplementedException();
        }

        public long ReadInt64(IntPtr address) {
            throw new NotImplementedException();
        }

        public ulong ReadUInt64(IntPtr address) {
            throw new NotImplementedException();
        }

        public float ReadSingle(IntPtr address) {
            throw new NotImplementedException();
        }

        public double ReadDouble(IntPtr address) {
            throw new NotImplementedException();
        }

        public IntPtr ReadIntPtr(IntPtr address) {
            throw new NotImplementedException();
        }

        public string ReadUnicodeString(IntPtr address, int length) {
            throw new NotImplementedException();
        }

        public PointerMemory GetIntPtr(IntPtr address) {
            throw new NotImplementedException();
        }
        
        private byte[] Update(IntPtr address, int length) {
            var bytes = memory.ReadByte(address, length);
            snapshot[address] = bytes;
            return bytes;
        }

        private bool TryResolve(IntPtr address, int length, out byte[] bytes) {
            if (snapshot.ContainsKey(address)) {
                var snapshotBytes = snapshot[address];
                if (snapshotBytes.Length >= length) {
                    bytes = new byte[length];
                    Array.Copy(snapshotBytes, 0, bytes, 0, length);
                    return true;
                }
            }

            bytes = Array.Empty<byte>();
            return false;
        }
    }
}
