﻿using System;

namespace Memory {
    public class PointerMemory : MemoryBase {
        public PointerMemory(IntPtr handle, long moduleOffset) : base(handle, moduleOffset) {
        }
    }
}