﻿using System;
using System.Text;
using Memory.Native;

namespace Memory {
    public class ProcessModuleMemory : MemoryBase {
        public ProcessModuleMemory(IntPtr process, long moduleOffset) : base(process, moduleOffset) {
        }
    }
}