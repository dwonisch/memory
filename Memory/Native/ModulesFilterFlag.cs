﻿using System;

namespace Memory.Native {
    [Flags]
    public enum ModulesFilterFlag {
        Default = 0x00,
        Modules_32bit = 0x01,
        Modules_64bit = 0x02,
        All = 0x03
    }
}
