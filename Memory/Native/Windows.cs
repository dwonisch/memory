﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Memory.Native {
    internal static class Windows {
        // https://docs.microsoft.com/en-us/windows/win32/api/memoryapi/nf-memoryapi-readprocessmemory
        [DllImport("kernel32.dll")]
        public static extern bool ReadProcessMemory(IntPtr process, IntPtr baseAddress, [Out] byte[] buffer, IntPtr size, out uint numRead);

        [DllImport("kernel32.dll")]
        public static extern bool WriteProcessMemory(IntPtr process, IntPtr baseAddress, byte[] buffer, int size, out uint numWrite);

        [DllImport("kernel32.dll")]
        public static extern IntPtr OpenProcess(int dwDesiredAccess, bool bInheritHandler, int dwProcessId);

        [DllImport("kernel32.dll")] 
        public static extern int CloseHandle(IntPtr hObject);

        [DllImport("psapi.dll")]
        public static extern bool EnumProcessModulesEx(IntPtr process, IntPtr[] module, int cb, out int lpcbNeeded, ModulesFilterFlag flags);

        [DllImport("psapi.dll")]
        public static extern bool GetModuleInformation(IntPtr process, IntPtr module, out ModuleInfo moduleEntry, int cb);
        
        [DllImport("psapi.dll")]
        public static extern int GetModuleBaseNameA(IntPtr process, IntPtr module, byte[] moduleName, int size);

        [DllImport("psapi.dll")]
        public static extern int GetProcessImageFileNameA(IntPtr process, [Out] byte[] processPath, int size);

        [DllImport("kernel32.dll")]
        public static extern int QueryDosDevice(string driveName, StringBuilder deviceName, int size);

    }
}
