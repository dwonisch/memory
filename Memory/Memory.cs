﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using Memory.Native;

namespace Memory
{
    public static class Memory
    {
        public static byte ReadByte(IntPtr process, IntPtr address) {
            return ReadByte(process, address, sizeof(byte)).First();
        }

        public static short ReadInt16(IntPtr process, IntPtr address) {
            return BitConverter.ToInt16(ReadByte(process, address, sizeof(short)), 0);
        }

        public static ushort ReadUInt16(IntPtr process, IntPtr address) {
            return BitConverter.ToUInt16(ReadByte(process, address, sizeof(ushort)), 0);
        }

        public static int ReadInt32(IntPtr process, IntPtr address) {
            return BitConverter.ToInt32(ReadByte(process, address, sizeof(int)), 0);
        }

        public static uint ReadUInt32(IntPtr process, IntPtr address) {
            return BitConverter.ToUInt32(ReadByte(process, address, sizeof(uint)), 0);
        }

        public static long ReadInt64(IntPtr process, IntPtr address) {
            return BitConverter.ToInt64(ReadByte(process, address, sizeof(long)), 0);
        }

        public static ulong ReadUInt64(IntPtr process, IntPtr address) {
            return BitConverter.ToUInt64(ReadByte(process, address, sizeof(ulong)), 0);
        }

        public static float ReadSingle(IntPtr process, IntPtr address) {
            return BitConverter.ToSingle(ReadByte(process, address, sizeof(float)), 0);
        }

        public static double ReadDouble(IntPtr process, IntPtr address) {
            return BitConverter.ToDouble(ReadByte(process, address, sizeof(double)), 0);
        }

        public static IntPtr ReadIntPtr(IntPtr process, IntPtr address) {
            var readByte = ReadByte(process, address, IntPtr.Size);
            if(IntPtr.Size <= 4)
                return new IntPtr(BitConverter.ToInt32(readByte, 0));
            return new IntPtr(BitConverter.ToInt64(readByte, 0));
        }

        public static int[] ReadInt32(IntPtr process, IntPtr address, int length) {
            var result = new int[length];
            var buffer = ReadByte(process, address, length * 4);

            for (int i = 0; i < length; i++) {
                result[i] = BitConverter.ToInt32(buffer, i * 4);
            }

            return result;
        }

        public static byte[] ReadByte(IntPtr process, IntPtr address, int length) {
            var buffer = new byte[length];
            Windows.ReadProcessMemory(process, address, buffer, (IntPtr)length, out uint numRead);
            return buffer;
        }

        public static void WriteByte(IntPtr process, IntPtr address, byte[] data) {
            Windows.WriteProcessMemory(process, address, data, data.Length, out uint numWrite);
        }

        public static bool GetFlag(byte value, int flag) {
            return (value & (1 << flag % 8)) != 0;
        }

        public static bool GetFlag(int value, int flag) {
            return (value & (1 << flag % 32)) != 0;
        }

        public static bool GetFlag(long value, int flag) {
            return (value & (1 << flag % 64)) != 0;
        }

        public static bool GetFlag(byte[] value, int flag) {
            var i = flag / 8;
            if (i >= value.Length)
                throw new IndexOutOfRangeException();

            return (value[i] & (1 << flag % 8)) != 0;
        }

        public static ModuleInfo GetModuleInformation(IntPtr process, IntPtr module) {
            Windows.GetModuleInformation(process, module, out var moduleInfo, Marshal.SizeOf<ModuleInfo>());
            return moduleInfo;
        }

        public static string GetModuleName(IntPtr process, IntPtr module, int maxLength) {

            var name = new byte[maxLength];
            var length = Windows.GetModuleBaseNameA(process, module, name, name.Length);

            return Encoding.Default.GetString(name, 0, (int)length);
        }
    }
}
