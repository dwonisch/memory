﻿using System;

namespace Memory {
    public interface IMemory {
        byte ReadByte(IntPtr address);
        byte[] ReadByte(IntPtr address, int length);
        short ReadInt16(IntPtr address);
        ushort ReadUInt16(IntPtr address);
        int ReadInt32(IntPtr address);
        uint ReadUInt32(IntPtr address);
        long ReadInt64(IntPtr address);
        ulong ReadUInt64(IntPtr address);
        float ReadSingle(IntPtr address);
        double ReadDouble(IntPtr address);
        IntPtr ReadIntPtr(IntPtr address);
        string ReadUnicodeString(IntPtr address, int length);
        PointerMemory GetIntPtr(IntPtr address);
    }
}