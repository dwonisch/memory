﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Memory {
    public static class IntPtrExtensions {
        public static IntPtr Add(this IntPtr ptr, long offset) {
            return new IntPtr(ptr.ToInt64() + offset);
        }
    }
}
