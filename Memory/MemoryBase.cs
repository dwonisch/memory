﻿using System;
using System.Text;

namespace Memory {
    public abstract class MemoryBase : IMemory {
        protected readonly IntPtr handle;
        protected readonly long moduleOffset;

        protected MemoryBase(IntPtr handle, long moduleOffset) {
            this.handle = handle;
            this.moduleOffset = moduleOffset;
        }

        public byte ReadByte(IntPtr address) {
            return Memory.ReadByte(handle, address);
        }

        public byte[] ReadByte(IntPtr address, int length) {
            return Memory.ReadByte(handle, address, length);
        }

        public short ReadInt16(IntPtr address) {
            return Memory.ReadInt16(handle, address);
        }
        public ushort ReadUInt16(IntPtr address) {
            return Memory.ReadUInt16(handle, address);
        }

        public int ReadInt32(IntPtr address) {
            return Memory.ReadInt32(handle, ToLocalOffset(address));
        }

        public uint ReadUInt32(IntPtr address) {
            return Memory.ReadUInt32(handle, address);
        }

        public long ReadInt64(IntPtr address) {
            return Memory.ReadInt64(handle, ToLocalOffset(address));
        }

        public ulong ReadUInt64(IntPtr address) {
            return Memory.ReadUInt64(handle, address);
        }

        public float ReadSingle(IntPtr address) {
            return Memory.ReadSingle(handle, address);
        }

        public double ReadDouble(IntPtr address) {
            return Memory.ReadDouble(handle, address);
        }

        public IntPtr ReadIntPtr(IntPtr address) {
            return Memory.ReadIntPtr(handle, ToLocalOffset(address));
        }

        public string ReadUnicodeString(IntPtr address, int length) {
            return Encoding.Unicode.GetString(Memory.ReadByte(handle, address, length * 2));
        }

        public PointerMemory GetIntPtr(IntPtr address) {
            return new PointerMemory(handle, Memory.ReadIntPtr(handle, ToLocalOffset(address)).ToInt64());
        }

        private IntPtr ToLocalOffset(IntPtr address) {
            return address.Add(moduleOffset);
        }
    }
}