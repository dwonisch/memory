﻿using System;

namespace Memory {
    public struct ModuleInfo {
        public IntPtr Base { get; set; }
        public uint Size { get; set; }
        public IntPtr EntryPoint { get; set; }
    }
}